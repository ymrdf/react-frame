import React from 'react';
import { Router , Route } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';

import store from './store';

import Page1 from './pages/page1.js';
import Page2 from './pages/page2.js';


const history = createBrowserHistory();

console.log(store);

const routes =()=>{
  return (
    <Router history={history}>
      <div>
        <Route path="/page1" component={Page1}/>
        <Route path="/page2" component={Page2}/>
      </div>
    </Router>
  );
};

export default routes;
