export const user = (state, action) => {
  console.log(action);
  switch (action.type) {
    case 'SETNAME':
      return Object.assign({}, state, { name: action.name });
    default:
      return { ...state };
  }
};