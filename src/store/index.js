import { createStore, combineReducers } from 'redux';
import * as reducers from './reducers';

const defaultState = {
  user:{
    name:'dennis'
  }
};


const reducer = combineReducers(reducers);
const store = createStore(reducer,defaultState);
export default store;