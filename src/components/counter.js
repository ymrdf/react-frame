/**
 * Button counter
 */
import React, { Component } from 'react';

class Counter extends Component {
  constructor(props){
    super(props);
    this.state={
      number:props.number
    }
  }

  addOne = () => {
    this.setState({
      number:this.state.number+1
    })
  }

  render () {
    return (
      <div>
        <div>{ this.state.number }</div>
        <button onClick={ this.addOne }>add one</button>
      </div>
    )
  }
}

export default Counter;