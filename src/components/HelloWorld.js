/**
 * Button HelloWorld
 */
import React from 'react';

const HelloWorld = (props) => {
  return (
    <div>{ props.name } say:Hello Wold!</div>
  )
};

export default HelloWorld;