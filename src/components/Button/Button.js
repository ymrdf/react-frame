/**
 * Button component
 */
import React from 'react';

import './button.css';

const Button = (props) => {
  const { children, loading, submit } = props;
  return (
    <button onClick={submit} disabled={ loading ? 'disabled' : null }>
      { loading && <i className="loading"></i> }
      { children }
    </button>
  )

};

export default Button;