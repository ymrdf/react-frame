/**
 * Test page two
 */
import React, { Component } from 'react';

import HelloWorld from 'Components/HelloWorld.js';
import Counter from 'Components/counter.js';


class Page2 extends Component {
  render() {
    return (
      <div className="Page2">
        <h1>page2</h1>
        <HelloWorld name="I"/>
        <Counter number={1} />
      </div>
    );
  }
}

export default Page2;