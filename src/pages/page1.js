/**
 * Test page one;
 */
import React, { Component } from 'react';

import store from '../store';

import Button from 'Components/Button/Button.js';

class Page1 extends Component {
  state={
    name:store.getState().user.name,
    newName:''
  }

  componentDidMount (props) {
    store.subscribe(()=>{
      this.setState({
        name:store.getState().user.name
      })
    })
  }

  setName () {
    store.dispatch({type:'SETNAME',name:'gao'});
  }

  newName (e) {
    this.setState({
      newName:e.keyCode()
    })
  }

  render () {
    return (
      <div className="Page1">
        <h1>page1</h1>
        <div>
          { this.state.name }
          <input onInput={ this.newName }></input>
          <Button submit={ this.setName('gao') } loading={ true }>
            setName
          </Button>
        </div>
      </div>
    );
  }
}

export default Page1;